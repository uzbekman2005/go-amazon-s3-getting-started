package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	r := gin.Default()

	// Setup gin app
	r.Static("/assets", "./assets")
	r.LoadHTMLGlob("templates/*")
	r.MaxMultipartMemory = 8 << 20 // 8 MiB

	// setup s3 uploader
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		log.Fatal("Error while setting up s3 uploader: %w", err)
		return
	}

	client := s3.NewFromConfig(cfg)
	uploader := manager.NewUploader(client)
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})
	})

	r.POST("/", func(c *gin.Context) {
		file, err := c.FormFile("image")
		if err != nil {
			fmt.Println(err)
			c.HTML(http.StatusBadRequest, "index.html", gin.H{
				"error": err,
			})
			return
		}

		f, openErr := file.Open()
		if openErr != nil {
			fmt.Println(err)
			c.HTML(http.StatusBadRequest, "index.html", gin.H{
				"error": "Failed to upload image",
			})
			return
		}

		result, err := uploader.Upload(context.TODO(), &s3.PutObjectInput{
			Bucket: aws.String("adds-bucket"),
			Key:    aws.String(file.Filename),
			Body:   f,
			ACL:    "public-read",
		})
		if err != nil {
			fmt.Println(err)
			c.HTML(http.StatusBadRequest, "index.html", gin.H{
				"error": "Failed to upload image",
			})
			return
		}
		fmt.Println(result.Location)
		c.HTML(http.StatusOK, "index.html", gin.H{
			"image": result.Location,
		})
	})
	r.Run()
}
